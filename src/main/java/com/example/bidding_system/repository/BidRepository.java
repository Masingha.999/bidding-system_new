package com.example.bidding_system.repository;

import com.example.bidding_system.model.BidInformation;
import com.example.bidding_system.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BidRepository extends JpaRepository<BidInformation, String> {

    BidInformation findByProduct(Product product);
}
