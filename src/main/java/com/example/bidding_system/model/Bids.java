package com.example.bidding_system.model;

import javax.persistence.*;

@Entity
@Table(name = "bids")

public class Bids {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "f_name")
    private String f_Name;

    @Column(name = "l_name")
    private String l_Name;

    @Column(name = "bidamount")
    private Double amount;

    public Bids(long id, String f_Name, String l_Name, Double amount) {
        super();
        this.id = id;
        this.f_Name = f_Name;
        this.l_Name = l_Name;
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getF_Name() {
        return f_Name;
    }

    public void setF_Name(String f_Name) {
        this.f_Name = f_Name;
    }

    public String getL_Name() {
        return l_Name;
    }

    public void setL_Name(String l_Name) {
        this.l_Name = l_Name;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }


}
