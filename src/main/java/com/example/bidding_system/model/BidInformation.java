package com.example.bidding_system.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "bidinfo" )
public class BidInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    public BidInformation(){

    }

    @Column(name = "start_time")
    private Date startTime;

    @Column(name = "end_time")
    private Date endTime;

    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    private Set<User> participants;

    @Column(name = "bids")
    private List<Bids> bidding;

    public BidInformation(long id, Product product, Date startTime, Date endTime, Set<User> participants, List<Bids> bidding) {
        super();
        this.id = id;
        this.product = product;
        this.startTime = startTime;
        this.endTime = endTime;
        this.participants = participants;
        this.bidding = bidding;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Set<User> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<User> participants) {
        this.participants = participants;
    }

    public List<Bids> getBidding() {
        return bidding;
    }

    public void setBidding(List<Bids> bidding) {
        this.bidding = bidding;
    }

    @Override
    public String toString() {
        return "BidInformation{" +
                "id=" + id +
                ", product=" + product +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", participants=" + participants +
                ", bidding=" + bidding +
                '}';
    }
}
