package com.example.bidding_system.model;
import javax.persistence.*;
import java.util.Set;

@Entity
@Table (name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "product_name")
    private String name;

    @Column(name = "product_disc")
    private String disc;

    @Column(name = "price")
    private Double price;

    @OneToMany(mappedBy = "product", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<BidInformation> bidInformationSet;

    public Product(){

    }


    public Product(long id, String name, String disc, Double price) {
        super();
        this.id = id;
        this.name = name;
        this.disc = disc;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisc() {
        return disc;
    }

    public void setDisc(String disc) {
        this.disc = disc;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
