package com.example.bidding_system.services.impl;

import com.example.bidding_system.model.User;
import com.example.bidding_system.repository.UserRepository;
import com.example.bidding_system.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User getById(String id) throws Exception {
        return null;
    }

    @Override
    public User addNewUser(User user) throws Exception {
        return null;
    }
}
