package com.example.bidding_system.services;

import com.example.bidding_system.model.BidInformation;

public interface BidInformationService {

    BidInformation getById (String id) throws Exception;
}
