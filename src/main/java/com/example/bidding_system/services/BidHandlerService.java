package com.example.bidding_system.services;

import com.example.bidding_system.model.BidInformation;
import com.example.bidding_system.model.Bids;

import javax.xml.ws.Response;
import java.util.List;

public interface BidHandlerService {

    Response<?> addBid(String productId) throws Exception;

    List<BidInformation> getAllBids() throws Exception;

    Bids getWinners(String productId) throws Exception;
}
