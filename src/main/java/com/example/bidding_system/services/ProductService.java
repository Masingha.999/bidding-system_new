package com.example.bidding_system.services;

import com.example.bidding_system.model.Product;

public interface ProductService {

    Product getById(String id) throws Exception;

    Product addNewProduct(Product product) throws Exception;
}
