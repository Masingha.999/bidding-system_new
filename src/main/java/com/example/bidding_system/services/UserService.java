package com.example.bidding_system.services;

import com.example.bidding_system.model.User;

public interface UserService {

    User getById(String id)throws Exception;

    User addNewUser(User user) throws Exception;
}
